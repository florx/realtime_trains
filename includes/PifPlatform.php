<?php

class PifPlatform extends Dao{
	
	protected $primaryKey = 'id';
	protected $tableName = 'pif_platform';

	protected $pifRecord = array(
		'tiploc',
		'platform_id',
		'start_date',
		'end_date',
		'length',
		'power_supply_type',
		'doo_passenger',
		'doo_non_passenger'
	);
	
}
