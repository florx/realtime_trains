<?php

class PifTiming extends Dao{
	
	protected $primaryKey = 'id';
	protected $tableName = 'pif_timing';

	protected $pifRecord = array(
		'tiploc_origin',
		'tiploc_destination',
		'running_line_code',
		'traction_type',
		'trailing_load',
		'speed',
		'ra_guage',
		'entry_speed',
		'exit_speed',
		'start_date',
		'end_date',
		'section_running_time',
		'description'
	);
	
}
