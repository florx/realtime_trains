<?php

class TrainActivation extends Dao{
	
	protected $primaryKey = 'id';
	protected $tableName = 'activations';
	
	protected function afterPersist(){
		
		$mysqli = $this->db->getConn();

		if (!($stmt = $mysqli->prepare("INSERT into rt_services (train_uid, train_id, train_service_code, stanox, schedule_id, schedule_location_id, activation_id)
			SELECT a.train_uid, a.train_id, a.train_service_code, stanox, schedule_id, sl.id as schedule_location_id, a.id as activation_id from schedules s
			join schedules_locations sl on s.id = sl.schedule_id
			left join locations ld on ld.tiploc = sl.tiploc_code
			left join activations a on a.train_uid = sl.CIF_train_uid
			where a.id = ?
			and s.schedule_start_date <= CURDATE() and s.schedule_end_date >= CURDATE() and s.CIF_stp_indicator = (
				select min(CIF_stp_indicator) from schedules where CIF_train_uid = a.train_uid and schedule_start_date <= CURDATE() and schedule_end_date >= CURDATE() 
			)"))) {
			throw new DaoException ("Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error);
		}

		/* Prepared statement, stage 2: bind and execute */
		if (!$stmt->bind_param("i", $this->id)) {
			throw new DaoException ("Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error);
		}

		if (!$stmt->execute()) {
			throw new DaoException ("Execute failed: (" . $stmt->errno . ") " . $stmt->error);
		}
	}
	
}