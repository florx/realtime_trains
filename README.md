Train Times
===========

The beginnings of a Real Time, train schedule website.

##Getting Started

### Dependencies

You need the STOMP package from PECL.

`pecl install stomp`

### Configuration

Rename the `config.example.php` file to `config.php`
Fill in all of the blank fields.

### Importing Data

You'll need to get some files together before you can import. See the Wiki (http://nrodwiki.rockshore.net) for details on where to obtain them.

Run each of the `import*.php` files. Some of these will take a few hours to import everything (especially the Schedule import).
You may need to split the schedule file up as PHP cannot handle files over 2GB (if compiled without the 64bit flag)

#Licence

    Copyright 2015 Jake Hall
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.