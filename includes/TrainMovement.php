<?php

class TrainMovement extends Dao{
	
	protected $primaryKey = 'id';
	protected $tableName = 'movements';
	
	protected function afterPersist(){
		
		$mysqli = $this->db->getConn();

		if (!($stmt = $mysqli->prepare("UPDATE rt_services rs 
			join movements m on rs.train_id = m.train_id and rs.stanox = m.loc_stanox and event_type = 'DEPARTURE'
			set rs.trust_departure = m.actual_timestamp, rs.trust_departure_id = m.id
			where m.id = ?"))) {
			throw new DaoException ("Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error);
		}

		/* Prepared statement, stage 2: bind and execute */
		if (!$stmt->bind_param("i", $this->id)) {
			throw new DaoException ("Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error);
		}

		if (!$stmt->execute()) {
			throw new DaoException ("Execute failed: (" . $stmt->errno . ") " . $stmt->error);
		}

		if (!($stmt = $mysqli->prepare("UPDATE rt_services rs 
			join movements m on rs.train_id = m.train_id and rs.stanox = m.loc_stanox and event_type = 'ARRIVAL'
			set rs.trust_arrival = m.actual_timestamp, rs.trust_arrival_id = m.id
			where m.id = ?"))) {
			throw new DaoException ("Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error);
		}

		/* Prepared statement, stage 2: bind and execute */
		if (!$stmt->bind_param("i", $this->id)) {
			throw new DaoException ("Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error);
		}

		if (!$stmt->execute()) {
			throw new DaoException ("Execute failed: (" . $stmt->errno . ") " . $stmt->error);
		}
	}
}