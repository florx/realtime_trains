<?php

class PifLocation extends Dao{
	
	protected $primaryKey = 'id';
	protected $tableName = 'pif_location';

	protected $pifRecord = array(
		'tiploc',
		'location_name',
		'start_date',
		'end_date',
		'os_easting',
		'os_northing',
		'timing_point_type',
		'zone',
		'stanox',
		'off_network_ind',
		'force_lpb'
	);
	
}
