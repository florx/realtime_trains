-- --------------------------------------------------------
-- Host:                         florxlabs.com
-- Server version:               5.1.52 - MySQL Community Server (GPL) by Utter Ramblings
-- Server OS:                    redhat-linux-gnu
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table trains.activations
CREATE TABLE IF NOT EXISTS `activations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `train_id` varchar(250) DEFAULT NULL COMMENT 'The 10-character unique identity for this train',
  `creation_timestamp` datetime DEFAULT NULL COMMENT 'The timestamp (in milliseconds since the UNIX epoch) when the train was originally created in TRUST',
  `tp_origin_timestamp` date DEFAULT NULL COMMENT 'The date, in YYYY-MM-DD format, that the train runs. For trains activated before midnight that run after midnight, this date will be tomorrow''s date.',
  `train_uid` varchar(250) DEFAULT NULL COMMENT 'The unique ID of the schedule being activated - either a letter and five numbers, or a space and five numbers for VSTP trains',
  `sched_origin_stanox` varchar(250) DEFAULT NULL COMMENT 'STANOX code for the originating location in the schedule',
  `schedule_start_date` date DEFAULT NULL COMMENT 'The start date of the schedule',
  `schedule_end_date` date DEFAULT NULL COMMENT 'The end date of the schedule',
  `schedule_source` varchar(250) DEFAULT NULL COMMENT 'Set to C for schedules from CIF/ITPS, or V for schedules from VSTP/TOPS',
  `schedule_type` varchar(250) DEFAULT NULL COMMENT 'Either C (Cancellation), N (New STP), O (STP Overlay) or P (Permanent i.e. as per the WTT/LTP)',
  `schedule_wtt_id` varchar(250) DEFAULT NULL COMMENT 'The signaling ID (headcode) and speed class of the train',
  `d1266_record_number` varchar(250) DEFAULT NULL COMMENT 'Either 00000 for a CIF/ITPS schedule, or the TOPS unique ID of the schedule',
  `tp_origin_stanox` varchar(250) DEFAULT NULL COMMENT 'The STANOX code of the origin of the train',
  `origin_dep_timestamp` datetime DEFAULT NULL COMMENT 'The WTT time of departure from the originating location. A UNIX timestamp in milliseconds since the UNIX epoch, in UTC.',
  `train_call_type` varchar(250) DEFAULT NULL COMMENT 'Either AUTOMATIC for auto-called trains, or MANUAL for manual-called trains',
  `train_call_mode` varchar(250) DEFAULT NULL COMMENT 'Set to NORMAL for a train called normally, or OVERNIGHT if the train is called as part of an overnight batch process to activate peak period trains early',
  `toc_id` varchar(250) DEFAULT NULL COMMENT 'Operating company ID as per TOC Codes',
  `train_service_code` varchar(250) DEFAULT NULL COMMENT 'Train service code as per schedule',
  `train_file_address` varchar(250) DEFAULT NULL COMMENT 'The TOPS train file address, if applicable',
  PRIMARY KEY (`id`),
  KEY `train_uid` (`train_uid`),
  KEY `train_id` (`train_id`),
  KEY `schedule_wtt_id` (`schedule_wtt_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table trains.berth_step
CREATE TABLE IF NOT EXISTS `berth_step` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area_id` varchar(50) NOT NULL DEFAULT '0',
  `to` varchar(4) NOT NULL DEFAULT '0',
  `from` varchar(4) NOT NULL DEFAULT '0',
  `descr` varchar(4) NOT NULL DEFAULT '0',
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `to` (`to`),
  KEY `from` (`from`),
  KEY `descr` (`descr`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table trains.cancellations
CREATE TABLE IF NOT EXISTS `cancellations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `train_id` varchar(250) DEFAULT NULL COMMENT 'The 10-character unique identity for this train (sent in the TRUST activation message)',
  `orig_loc_stanox` varchar(250) DEFAULT NULL COMMENT 'For an an "OUT OF PLAN" cancellation, this is the location that the train should have been at according to the schedule',
  `orig_loc_timestamp` varchar(250) DEFAULT NULL COMMENT 'For an "OUT OF PLAN" cancellation, this is the departure time of the location that the train should have been at according to the schedule',
  `toc_id` varchar(250) DEFAULT NULL COMMENT 'Operating company ID as per TOC Codes',
  `dep_timestamp` datetime DEFAULT NULL COMMENT 'The departure time at the location that the train is cancelled from (in milliseconds since the UNIX epoch)',
  `division_code` varchar(250) DEFAULT NULL COMMENT 'Operating company ID as per TOC Codes',
  `loc_stanox` varchar(250) DEFAULT NULL COMMENT 'The STANOX of the location that the train is being cancelled from. For an "OUT OF PLAN" cancellation, this STANOX will not be in the schedule, but a Train Movement message will have already been sent.',
  `canx_timestamp` datetime DEFAULT NULL COMMENT 'The time at which the cancellation was input to TRUST',
  `canx_reason_code` varchar(250) DEFAULT NULL COMMENT 'The reason code for the cancellation, taken from the Delay Attribution Guide',
  `canx_type` varchar(250) DEFAULT NULL COMMENT 'Either "ON CALL" for a planned cancellation, "AT ORIGIN", "EN ROUTE" or "OUT OF PLAN"',
  `train_service_code` varchar(250) DEFAULT NULL COMMENT 'Train service code as per the schedule',
  `train_file_address` varchar(250) DEFAULT NULL COMMENT 'The TOPS train file address, if applicable',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table trains.locations
CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stanox` int(11) NOT NULL DEFAULT '0',
  `uic` varchar(6) NOT NULL DEFAULT '0',
  `3alpha` varchar(3) NOT NULL DEFAULT '0',
  `tiploc` varchar(50) NOT NULL DEFAULT '0',
  `nlc` varchar(6) NOT NULL DEFAULT '0',
  `nlcdesc` varchar(250) NOT NULL DEFAULT '0',
  `nlcdesc16` varchar(16) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `tiploc` (`tiploc`),
  KEY `stanox` (`stanox`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table trains.location_data
CREATE TABLE IF NOT EXISTS `location_data` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `location` varchar(255) DEFAULT NULL,
  `crs` varchar(255) DEFAULT NULL,
  `nlc` varchar(255) DEFAULT NULL,
  `tiploc` varchar(255) DEFAULT NULL,
  `stanox` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `stanox` (`stanox`),
  KEY `location` (`location`),
  KEY `crs` (`crs`),
  KEY `nlc` (`nlc`),
  KEY `tiploc` (`tiploc`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table trains.movements
CREATE TABLE IF NOT EXISTS `movements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `train_id` varchar(250) DEFAULT NULL COMMENT 'The 10-character unique identity for this train at TRUST activation time',
  `actual_timestamp` datetime DEFAULT NULL COMMENT 'The date and time that this event happened at the location',
  `loc_stanox` varchar(250) DEFAULT NULL COMMENT 'The STANOX of the location at which this event happened',
  `gbtt_timestamp` datetime DEFAULT NULL COMMENT 'The planned GBTT (passenger) date and time that the event was due to happen at this location',
  `planned_timestamp` datetime DEFAULT NULL COMMENT 'The planned date and time that this event was due to happen at this location',
  `original_loc_stanox` varchar(250) DEFAULT NULL COMMENT 'If the location has been revised, the STANOX of the location in the schedule at activation time',
  `original_loc_timestamp` varchar(250) DEFAULT NULL COMMENT 'The planned departure time associated with the original location',
  `planned_event_type` varchar(250) DEFAULT NULL COMMENT 'The planned type of event - one of "ARRIVAL", "DEPARTURE" or "DESTINATION"',
  `event_type` varchar(250) DEFAULT NULL COMMENT 'The type of event - either "ARRIVAL" or "DEPARTURE"',
  `event_source` varchar(250) DEFAULT NULL COMMENT 'Whether the event source was "AUTOMATIC" from SMART, or "MANUAL" from TOPS or TRUST SDR',
  `correction_ind` tinyint(1) DEFAULT NULL COMMENT 'Set to "false" if this report is not a correction of a previous report, or "true" if it is',
  `offroute_ind` tinyint(1) DEFAULT NULL COMMENT 'Set to "false" if this report is for a location in the schedule, or "true" if it is not',
  `direction_ind` varchar(250) DEFAULT NULL COMMENT 'For automatic reports, either "UP" or "DOWN" depending on the direction of travel',
  `line_ind` varchar(250) DEFAULT NULL COMMENT 'A single character (or blank) depending on the line the train is travelling on, e.g. F = Fast, S = Slow',
  `platform` varchar(250) DEFAULT NULL COMMENT 'Two characters (including a space for a single character) or blank if the movement report is associated with a platform number',
  `route` varchar(250) DEFAULT NULL COMMENT 'A single character (or blank) to indicate the exit route from this location',
  `current_train_id` varchar(250) DEFAULT NULL COMMENT 'Where a train has had its identity changed, the current 10-character unique identity for this train',
  `train_service_code` varchar(250) DEFAULT NULL COMMENT 'Train service code as per schedule',
  `division_code` varchar(250) DEFAULT NULL COMMENT 'Operating company ID as per TOC Codes',
  `toc_id` varchar(250) DEFAULT NULL COMMENT 'Operating company ID as per TOC Codes',
  `timetable_variation` varchar(250) DEFAULT NULL COMMENT 'The number of minutes variation from the scheduled time at this location. Off-route reports will contain "0"',
  `variation_status` varchar(250) DEFAULT NULL COMMENT 'One of "ON TIME", "EARLY", "LATE" or "OFF ROUTE"',
  `next_report_stanox` varchar(250) DEFAULT NULL COMMENT 'The STANOX of the location at which the next report for this train is due',
  `next_report_run_time` varchar(250) DEFAULT NULL COMMENT 'The running time to the next location',
  `train_terminated` tinyint(1) DEFAULT NULL COMMENT 'Set to "true" if the train has completed its journey, or "false" otherwise',
  `delay_monitoring_point` tinyint(1) DEFAULT NULL COMMENT 'Set to "true" if this is a delay monitoring point, "false" if it is not. Off-route reports will contain "false"',
  `train_file_address` varchar(250) DEFAULT NULL COMMENT 'The TOPS train file address, if applicable',
  `reporting_stanox` varchar(250) DEFAULT NULL COMMENT 'The STANOX of the location that generated this report. Set to "00000" for manual and off-route reports',
  `auto_expected` tinyint(1) DEFAULT NULL COMMENT 'Set to "true" if an automatic report is expected for this location, otherwise "false"',
  PRIMARY KEY (`id`),
  KEY `train_id` (`train_id`),
  KEY `train_service_code` (`train_service_code`),
  KEY `loc_stanox` (`loc_stanox`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table trains.pif_location
CREATE TABLE IF NOT EXISTS `pif_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tiploc` varchar(7) NOT NULL DEFAULT '0',
  `location_name` varchar(32) NOT NULL DEFAULT '0',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `os_easting` int(11) NOT NULL DEFAULT '0',
  `os_northing` int(11) NOT NULL DEFAULT '0',
  `timing_point_type` varchar(1) NOT NULL DEFAULT '0',
  `zone` int(11) NOT NULL DEFAULT '0',
  `stanox` int(11) NOT NULL DEFAULT '0',
  `off_network_ind` tinyint(1) NOT NULL DEFAULT '0',
  `force_lpb` varchar(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table trains.pif_network
CREATE TABLE IF NOT EXISTS `pif_network` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tiploc_origin` varchar(7) NOT NULL DEFAULT '0',
  `tiploc_destination` varchar(7) NOT NULL DEFAULT '0',
  `running_line_code` varchar(3) NOT NULL DEFAULT '0',
  `running_line_desc` varchar(20) NOT NULL DEFAULT '0',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `initial_direction` varchar(1) NOT NULL DEFAULT '0',
  `final_direction` varchar(1) NOT NULL DEFAULT '0',
  `distance` int(11) NOT NULL DEFAULT '0',
  `doo_passenger` tinyint(1) NOT NULL DEFAULT '0',
  `doo_non_passenger` tinyint(1) NOT NULL DEFAULT '0',
  `retb` tinyint(1) NOT NULL DEFAULT '0',
  `zone` varchar(7) NOT NULL DEFAULT '0',
  `reversible_line` varchar(1) NOT NULL DEFAULT '0',
  `power_supply_type` varchar(7) NOT NULL DEFAULT '0',
  `ra` varchar(2) NOT NULL DEFAULT '0',
  `max_train_length` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table trains.pif_platform
CREATE TABLE IF NOT EXISTS `pif_platform` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tiploc` varchar(7) NOT NULL DEFAULT '0',
  `platform_id` varchar(3) NOT NULL DEFAULT '0',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `length` int(11) NOT NULL DEFAULT '0',
  `power_supply_type` varchar(5) NOT NULL DEFAULT '0',
  `doo_passenger` tinyint(1) NOT NULL DEFAULT '0',
  `doo_non_passenger` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table trains.pif_ref
CREATE TABLE IF NOT EXISTS `pif_ref` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(3) DEFAULT NULL,
  `val` varchar(3) DEFAULT NULL,
  `description` varchar(61) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table trains.pif_timing
CREATE TABLE IF NOT EXISTS `pif_timing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tiploc_origin` varchar(7) NOT NULL DEFAULT '0',
  `tiploc_destination` varchar(7) NOT NULL DEFAULT '0',
  `running_line_code` varchar(3) NOT NULL DEFAULT '0',
  `traction_type` varchar(6) NOT NULL DEFAULT '0',
  `trailing_load` varchar(5) NOT NULL DEFAULT '0',
  `speed` varchar(3) NOT NULL DEFAULT '0',
  `ra_gauge` varchar(3) NOT NULL DEFAULT '0',
  `entry_speed` int(11) NOT NULL DEFAULT '0',
  `exit_speed` int(11) NOT NULL DEFAULT '0',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `section_running_time` varchar(10) NOT NULL DEFAULT '0',
  `description` varchar(64) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table trains.pif_timing_load
CREATE TABLE IF NOT EXISTS `pif_timing_load` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `traction_type` varchar(6) NOT NULL DEFAULT '0',
  `trailing_load` int(11) NOT NULL DEFAULT '0',
  `speed` int(11) NOT NULL DEFAULT '0',
  `ra_gauge` varchar(3) NOT NULL DEFAULT '0',
  `description` varchar(64) NOT NULL DEFAULT '0',
  `itps_power_type` varchar(3) NOT NULL DEFAULT '0',
  `itps_load` varchar(4) NOT NULL DEFAULT '0',
  `limiting_speed` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table trains.rt_services
CREATE TABLE IF NOT EXISTS `rt_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `train_uid` varchar(10) NOT NULL,
  `train_id` varchar(10) NOT NULL,
  `train_service_code` int(11) NOT NULL,
  `stanox` int(11) DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `schedule_location_id` int(11) DEFAULT NULL,
  `activation_id` int(11) DEFAULT NULL,
  `rt_arrival` datetime DEFAULT NULL,
  `rt_departure` datetime DEFAULT NULL,
  `trust_arrival` datetime DEFAULT NULL,
  `trust_departure` datetime DEFAULT NULL,
  `rt_arrival_id` int(11) DEFAULT NULL,
  `rt_departure_id` int(11) DEFAULT NULL,
  `trust_arrival_id` int(11) DEFAULT NULL,
  `trust_departure_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `stanox` (`stanox`),
  KEY `schedule_id` (`schedule_id`),
  KEY `activation_id` (`activation_id`),
  KEY `train_uid` (`train_uid`),
  KEY `train_id` (`train_id`),
  KEY `train_service_code` (`train_service_code`),
  KEY `schedule_location_id` (`schedule_location_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table trains.schedules
CREATE TABLE IF NOT EXISTS `schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CIF_train_uid` varchar(6) NOT NULL DEFAULT '',
  `transaction_type` varchar(250) DEFAULT NULL,
  `schedule_start_date` date DEFAULT NULL,
  `schedule_end_date` date DEFAULT NULL,
  `schedule_days_runs` varchar(250) DEFAULT NULL,
  `CIF_bank_holiday_running` varchar(250) DEFAULT NULL,
  `train_status` varchar(250) DEFAULT NULL,
  `CIF_stp_indicator` varchar(250) DEFAULT NULL,
  `atoc_code` varchar(250) DEFAULT NULL,
  `applicable_timetable` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `CIF_train_uid` (`CIF_train_uid`),
  KEY `schedule_start_date` (`schedule_start_date`),
  KEY `schedule_end_date` (`schedule_end_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table trains.schedules_locations
CREATE TABLE IF NOT EXISTS `schedules_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule_id` int(11) NOT NULL DEFAULT '0',
  `CIF_train_uid` varchar(6) NOT NULL DEFAULT '0',
  `location_type` varchar(250) DEFAULT NULL,
  `record_identity` varchar(250) DEFAULT NULL,
  `tiploc_code` varchar(250) DEFAULT NULL,
  `tiploc_instance` varchar(250) DEFAULT NULL,
  `arrival` time DEFAULT NULL,
  `departure` time DEFAULT NULL,
  `pass` time DEFAULT NULL,
  `public_arrival` time DEFAULT NULL,
  `public_departure` time DEFAULT NULL,
  `platform` varchar(250) DEFAULT NULL,
  `line` varchar(250) DEFAULT NULL,
  `path` varchar(250) DEFAULT NULL,
  `engineering_allowance` varchar(250) DEFAULT NULL,
  `pathing_allowance` varchar(250) DEFAULT NULL,
  `performance_allowance` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tiploc_code` (`tiploc_code`),
  KEY `CIF_train_uid` (`CIF_train_uid`),
  KEY `schedule_id` (`schedule_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table trains.schedules_segments
CREATE TABLE IF NOT EXISTS `schedules_segments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CIF_train_uid` varchar(6) NOT NULL DEFAULT '0',
  `CIF_train_category` varchar(250) DEFAULT NULL,
  `signalling_id` varchar(250) DEFAULT NULL,
  `CIF_headcode` varchar(250) DEFAULT NULL,
  `CIF_course_indicator` varchar(250) DEFAULT NULL,
  `CIF_train_service_code` varchar(250) DEFAULT NULL,
  `CIF_business_sector` varchar(250) DEFAULT NULL,
  `CIF_power_type` varchar(250) DEFAULT NULL,
  `CIF_timing_load` varchar(250) DEFAULT NULL,
  `CIF_speed` varchar(250) DEFAULT NULL,
  `CIF_operating_characteristics` varchar(250) DEFAULT NULL,
  `CIF_train_class` varchar(250) DEFAULT NULL,
  `CIF_sleepers` varchar(250) DEFAULT NULL,
  `CIF_reservations` varchar(250) DEFAULT NULL,
  `CIF_connection_indicator` varchar(250) DEFAULT NULL,
  `CIF_catering_code` varchar(250) DEFAULT NULL,
  `CIF_service_branding` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table trains.smart_locations
CREATE TABLE IF NOT EXISTS `smart_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fromberth` varchar(4) NOT NULL DEFAULT '0',
  `toberth` varchar(4) NOT NULL DEFAULT '0',
  `fromline` varchar(2) NOT NULL DEFAULT '0',
  `toline` varchar(2) NOT NULL DEFAULT '0',
  `berthoffset` varchar(50) NOT NULL DEFAULT '0',
  `platform` varchar(50) NOT NULL DEFAULT '0',
  `event` varchar(1) NOT NULL DEFAULT '0',
  `route` varchar(50) NOT NULL DEFAULT '0',
  `stanox` int(11) NOT NULL DEFAULT '0',
  `stanme` varchar(50) NOT NULL DEFAULT '0',
  `steptype` varchar(1) NOT NULL DEFAULT '0',
  `comment` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `stanox` (`stanox`),
  KEY `fromberth` (`fromberth`),
  KEY `toberth` (`toberth`),
  KEY `event` (`event`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table trains.stanox_berth_area
CREATE TABLE IF NOT EXISTS `stanox_berth_area` (
  `stanox` int(6) NOT NULL,
  `area_id` char(2) NOT NULL,
  PRIMARY KEY (`stanox`,`area_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
