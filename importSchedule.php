<?php

require('includes/init.php');

$firstLine = true;
$i = 0;
$total = 550359;
$timeStarted = time();

$db = DatabaseConnection::getInstance();

$db->query('ALTER TABLE schedules DISABLE KEYS');
$db->query('ALTER TABLE schedules_segments DISABLE KEYS');
$db->query('ALTER TABLE schedules_locations DISABLE KEYS');

foreach(array('xaa', 'xab') as $file){

	$file_handle = fopen($file, "r");
	while (!feof($file_handle)) {
		$line = fgets($file_handle);
		if($firstLine){ $firstLine = false; continue; }
		
		$data = json_decode($line);

		$keys = array_keys((array) $data);

		switch($keys[0]){
			case 'JsonScheduleV1':

				$thisData = $data->$keys[0];
				//print_R($thisData);

				$i++;
				if($i % 100 == 0){
					$timeElapsed = time() - $timeStarted;
					$perSec = $i / $timeElapsed;
					$itemsLeft = $total - $i;
					echo number_format($i) . '/' . number_format($total) . ' (' . round(($i / $total * 100), 2) .'%)' . '. Elapsed: ' . number_format($timeElapsed) .'s. PerSec: ' . round($perSec) .'. MinsLeft: ' .
					round($itemsLeft / $perSec / 60) . "\n";
				}
				

				$a = new Schedule($thisData);
				$scheduleID = $a->persist();

				if(isset($thisData->schedule_segment)){

					$thisData->schedule_segment->CIF_train_uid = $thisData->CIF_train_uid;

					$a = new ScheduleSegment($thisData->schedule_segment);
					$a->persist();

					if(isset($thisData->schedule_segment->schedule_location)){

						foreach($thisData->schedule_segment->schedule_location as $locationData){
							$locationData->CIF_train_uid = $thisData->CIF_train_uid;
							$locationData->schedule_id = $scheduleID;
							$a = new ScheduleLocation($locationData);
							$a->persist();
						}
					}
				}

				break;
			default:
				break;
		}
		
	}
	fclose($file_handle);
}

$db->query('ALTER TABLE schedules ENABLE KEYS');
$db->query('ALTER TABLE schedules_segments ENABLE KEYS');
$db->query('ALTER TABLE schedules_locations ENABLE KEYS');