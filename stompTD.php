<?php

require('includes/init.php');


$con = new Stomp(STOMP_SERVER, STOMP_USER, STOMP_PASS, array('client-id' => STOMP_USER . 'TD_ALL_SIG_AREA'));
if (!$con) {
	die('Connection failed: ' . stomp_connect_error());
}

$con->subscribe("/topic/TD_ALL_SIG_AREA", array('activemq.subscriptionName' => STOMP_USER . 'TD_ALL_SIG_AREA'));

$time = '';
$messagesThisSecond = 0;

while($con){
	if ($con->hasFrame()){
		$msg = $con->readFrame();
		foreach (json_decode($msg->body) as $event) {

			$newTime = date('H:i:s');

			if($time != $newTime){
				echo ' (' . $messagesThisSecond . ' messages)';
				echo "\n[" . $newTime . ']: ';
				$time = $newTime;
				$messagesThisSecond = 0;
			}

			$messagesThisSecond++;

			$keys = array_keys((array) $event);
			$messageType = $keys[0];

			switch($messageType){
				case 'CA_MSG': //berth step
				case 'CB_MSG':
				case 'CC_MSG':
					echo substr($messageType, 0, 2);
					$a = new BerthStep($event->$messageType);
					$a->persist();
					break;
				case 'CT_MSG': //heartbeat, doesn't do anything?
				case 'SF_MSG':
				case 'SG_MSG':
				case 'SH_MSG':
					//not interested in S class messages.
					break;
				default:
					print_R($event);
					echo 'Skipping ' . $messageType . "\n";
					break;
			}
		}
		$con->ack($msg);
	}
}

die('Connection lost: ' . time());
?>