

SELECT * from schedules s
join schedules_locations sl on sl.CIF_train_uid = s.CIF_train_uid
left join locations ld on ld.tiploc = sl.tiploc_code
left join activations a on a.train_uid = sl.CIF_train_uid
left join berth_step bs on bs.descr = SUBSTRING(a.train_id, 3, 4)
left join smart_locations smla on smla.fromberth = bs.`from` and smla.toberth = bs.`to` and smla.stanox = ld.stanox and smla.`event` in ('A','C')
left join smart_locations smld on smld.fromberth = bs.`from` and smld.toberth = bs.`to` and smld.stanox = ld.stanox and smld.`event` in ('B','D')

where sl.CIF_train_uid = "W99238"



select * from berth_step bs
join smart_locations smla on smla.fromberth = bs.`from` and smla.toberth = bs.`to`
join activations a on bs.descr LIKE CONCAT(a.schedule_wtt_id, '_')
limit 100



select * from berth_step bs
join smart_locations smla on smla.fromberth = bs.`from` and smla.toberth = bs.`to`
join activations a on a.schedule_wtt_id LIKE CONCAT(bs.descr, '_')
join schedules s on s.CIF_train_uid = a.train_uid and s.schedule_start_date <= CURDATE() and s.schedule_end_date >= CURDATE() and s.CIF_stp_indicator = (
	select min(CIF_stp_indicator) from schedules where CIF_train_uid = a.train_uid and schedule_start_date <= CURDATE() and schedule_end_date >= CURDATE() 
)
join locations l on l.id = (
	select min(id) from locations where stanox = smla.stanox
)
join schedules_locations sl on sl.schedule_id = s.id and l.tiploc = sl.tiploc_code
where bs.descr = '2R11'
and s.CIF_train_uid = 'C22939'




SELECT sl.*, ld.nlcdesc as location, a.id as activation_id, ma.actual_timestamp as rt_arrival, md.actual_timestamp as rt_departure, ld.3alpha, ld.tiploc, ld.stanox from schedules s
join schedules_locations sl on s.id = sl.schedule_id
left join locations ld on ld.tiploc = sl.tiploc_code
left join activations a on a.train_uid = sl.CIF_train_uid
left join movements ma on ma.train_id = a.train_id and ma.event_type = "ARRIVAL" and (ma.reporting_stanox = ld.stanox or ma.loc_stanox = ld.stanox)
left join movements md on md.train_id = a.train_id and md.event_type = "DEPARTURE" and (md.reporting_stanox = ld.stanox or md.loc_stanox = ld.stanox)
where sl.CIF_train_uid = "C22939"
and s.schedule_start_date <= CURDATE() and s.schedule_end_date >= CURDATE() and s.CIF_stp_indicator = (
	select min(CIF_stp_indicator) from schedules where CIF_train_uid = a.train_uid and schedule_start_date <= CURDATE() and schedule_end_date >= CURDATE() 
)




insert into rt_services (train_uid, train_id, train_service_code, stanox, schedule_id, activation_id)
SELECT a.train_uid, a.train_id, a.train_service_code, stanox, schedule_id, a.id as activation_id from schedules s
join schedules_locations sl on s.id = sl.schedule_id
left join locations ld on ld.tiploc = sl.tiploc_code
left join activations a on a.train_uid = sl.CIF_train_uid
where a.id = 14871

and s.schedule_start_date <= CURDATE() and s.schedule_end_date >= CURDATE() and s.CIF_stp_indicator = (
	select min(CIF_stp_indicator) from schedules where CIF_train_uid = a.train_uid and schedule_start_date <= CURDATE() and schedule_end_date >= CURDATE() 
)



SELECT * from schedules s
join schedules_locations sl on s.id = sl.schedule_id
join locations ld on ld.tiploc = sl.tiploc_code
join activations a on a.train_uid = sl.CIF_train_uid
left join rt_services rs on rs.schedule_location_id = sl.id
where sl.CIF_train_uid = "W99050"
and s.schedule_start_date <= CURDATE() and s.schedule_end_date >= CURDATE() and s.CIF_stp_indicator = (
	select min(CIF_stp_indicator) from schedules where CIF_train_uid = a.train_uid and schedule_start_date <= CURDATE() and schedule_end_date >= CURDATE() 
)



select * from movements m 
join rt_services rs on rs.train_id = m.train_id and rs.stanox = m.loc_stanox and event_type = 'DEPARTURE'
where m.id = 256145
