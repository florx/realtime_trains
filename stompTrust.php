<?php

require('includes/init.php');

$con = new Stomp(STOMP_SERVER, STOMP_USER, STOMP_PASS, array('client-id' => STOMP_USER . 'TRAIN_MVT_ALL_TOC'));
if (!$con) {
	die('Connection failed: ' . stomp_connect_error());
}

$con->subscribe("/topic/TRAIN_MVT_ALL_TOC", array('activemq.subscriptionName' => STOMP_USER . 'TRAIN_MVT_ALL_TOC'));

$time = '';
$messagesThisSecond = 0;

while($con){
	if ($con->hasFrame()){
		$msg = $con->readFrame();
		foreach (json_decode($msg->body) as $event) {

			$newTime = date('H:i:s');

			if($time != $newTime){
				echo ' (' . $messagesThisSecond . ' messages)';
				echo "\n[" . $newTime . ']: ';
				$time = $newTime;
				$messagesThisSecond = 0;
			}

			$messagesThisSecond++;

			switch($event->header->msg_type){
				case '0001': //activation
					echo 1;
					$a = new TrainActivation($event->body);
					$a->persist();
					break;
				case '0002': //cancellation
					echo 2;
					$a = new TrainCancellation($event->body);
					$a->persist();
				case '0003': //movement
					echo 3;
					$a = new TrainMovement($event->body);
					$a->persist();
					break;
				default:
					print_R($event);
					echo 'Skipping ' . $event->header->msg_type . "\n";
					break;
			}
		}
		$con->ack($msg);
	}
}

die('Connection lost: ' . time());
?>