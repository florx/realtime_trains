<?php 

require ('includes/init.php');

$db = DatabaseConnection::getInstance();

/*$query = $db->query('SELECT * from schedules_locations sl
left join location_data ld on ld.tiploc = sl.tiploc_code
where sl.CIF_train_uid = "W99208"');*/

$query = $db->query('SELECT sl.*, ld.nlcdesc as location, a.id as activation_id, rs.trust_arrival, rs.trust_departure, rs.rt_arrival, rs.rt_departure, ld.3alpha, ld.tiploc, ld.stanox from schedules s
join schedules_locations sl on s.id = sl.schedule_id
join locations ld on ld.tiploc = sl.tiploc_code
join activations a on a.train_uid = sl.CIF_train_uid
left join rt_services rs on rs.schedule_location_id = sl.id
where sl.CIF_train_uid = "W97311"
and s.schedule_start_date <= CURDATE() and s.schedule_end_date >= CURDATE() and s.CIF_stp_indicator = (
	select min(CIF_stp_indicator) from schedules where CIF_train_uid = a.train_uid and schedule_start_date <= CURDATE() and schedule_end_date >= CURDATE() 
)');

echo '<table>';
echo '<thead>
			<tr>
				<th></th>
				<th class="metadata platform"></th>
				<th class="wtt time borderleft" colspan="2"><abbr title="Working Timetable">WTT</abbr></th>
				<th class="gbtt time borderleft" colspan="2"><abbr title="Public Timetable">Public</abbr></th>
				<th class="realtime time borderleft" colspan="3">Realtime</th> 
				<th class="metadata line path borderleft" colspan="2">Route</th>
				<th class="metadata allowances borderleft" colspan="3">Allowances</th>
			</tr>
		<tr>
			<th>Location</th>
			<th class="metadata platform span2">Pl</th>
			<th class="wtt time span3 borderleft">Arr</th>
			<th class="wtt time span3">Dep</th>
			<th class="gbtt time span3 borderleft">Arr</th>
			<th class="gbtt time span3">Dep</th>
			<th class="realtime time span3 borderleft">Arr</th>
			<th class="realtime time span3">Dep</th>
			<th class="realtime delay span2">Dly</th>
			<th class="metadata line span2 borderleft">Line</th>
			<th class="metadata path span2">Path</th>
			<th class="metadata allowances engineering span2 borderleft"><abbr title="Engineering allowance">Eng</abbr></th>
			<th class="metadata allowances pathing span2"><abbr title="Pathing allowance">Pth</abbr></th>
			<th class="metadata allowances performance span2"><abbr title="Performance allowance">Prf</abbr></th>
		</tr>
		</thead>
		<tbody>';

while ($column = $query->fetch_assoc()) {
	$column = (object) $column;
	echo '<tr>';
	$threeAlpha = '3alpha';
	echo '<td>' . $column->location . ' ['.$column->$threeAlpha.'] <small>'.$column->stanox. ' '.$column->tiploc.'</small></td>';
	echo '<td>' . $column->platform . '</td>';
	echo '<td>' . formatTime($column->arrival) . '</td>';
	echo '<td>' . formatTime($column->departure) . '</td>';
	echo '<td>' . formatTime($column->public_arrival) . '</td>';
	echo '<td>' . formatTime($column->public_departure) . '</td>';

	if(is_null($column->activation_id)){
		echo '<td colspan="3">Unavailable</td>';
	}else{
		if(!is_null($column->rt_arrival)){
			echo '<td>' . formatTimeB($column->rt_arrival) . '<small>R</small></td>';
		}elseif(!is_null($column->trust_arrival)){
			echo '<td>' . formatTimeB($column->trust_arrival) . '<small>T</small></td>';
		}else{
			echo '<td></td>';
		}
		if(!is_null($column->rt_departure)){
			echo '<td>' . formatTimeB($column->rt_departure) . '<small>R</small></td>';
		}elseif(!is_null($column->trust_arrival)){
			echo '<td>' . formatTimeB($column->trust_departure) . '<small>T</small></td>';
		}else{
			echo '<td></td>';
		}
		echo '<td>-</td>';
	}

	
	echo '<td>' . $column->line . '</td>';
	echo '<td>' . $column->path . '</td>';
	echo '<td>' . $column->engineering_allowance . '</td>';
	echo '<td>' . $column->pathing_allowance . '</td>';
	echo '<td>' . $column->performance_allowance . '</td>';

	echo '</tr>';
}

echo '</tbody></table>';

function formatTime($time){
	$str = substr($time, 0, -3);
	if(substr($time, -2) == '30'){
		$str .= '&frac12;';
	}
	return $str;
}

function formatTimeB($time){
	if(empty($time)) return '';
	$time = date('H:i:s', strtotime($time));

	$str = substr($time, 0, -3);
	if(substr($time, -2) == '30'){
		$str .= '&frac12;';
	}
	return $str;
}