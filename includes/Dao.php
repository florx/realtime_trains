<?php

class Dao{

	protected $primaryKey = null;
	protected $tableName = null;
	protected $model = array();
	protected $data = array();
	protected $pifRecord = array();
	public $db = null;
	public $id = null;


	public function Dao($data = null){

		if(is_null($this->primaryKey)){
			throw new DaoException("Primary Key name is null");
		}

		if(is_null($this->tableName)){
			throw new DaoException("Table name is null");
		}

		$this->db = DatabaseConnection::getInstance();

		$this->handleData($data);
	}

	public function convertPifData($line){

		$lineArray = explode("\t", $line);
		$array = array();

		foreach($this->pifRecord as $i => $key){
			$array[$key] = trim($lineArray[$i]);
		}

		return $array;
	}

	private function handleData($data){
		if(is_null($data)) return;

		if(count($this->pifRecord) > 0){
			$data = $this->convertPifData($data);
		}

		$data = (object) $data;

		$model = $this->db->getModel($this->tableName);

		foreach($model as $key => $column){
			if(isset($data->$key)){
				$this->data[$key] = $this->convertToColumnType($data->$key, $column->Type, $key);
			}
		}
	}

	private function convertToColumnType($str, $columnType, $key){
		if($columnType == 'tinyint(1)'){
			if($str == 'Y') return 1;
			if($str == 'N') return 0;
			return filter_var($str, FILTER_VALIDATE_BOOLEAN);
		}elseif($columnType == 'datetime'){
			if(empty($str)) return '';
			return date('Y-m-d H:i:s', intval(substr($str, 0, 10)));
		}elseif($columnType == 'date'){
			if(empty($str)) return '';
			return date('Y-m-d', strtotime($str));
		}elseif($columnType == 'time'){
			if(strlen($str) == 4){
				return substr($str, 0, 2) . ':' . substr($str, 2, 2) . ':00';
			}else{
				return substr($str, 0, 2) . ':' . substr($str, 2, 2) . ':30';
			}
		}

		return $str;
	}

	public function persist(){
		$this->insert();

		$this->id = $this->db->insertID();
		$this->afterPersist();
		return $this->id;
	}

	protected function afterPersist(){
		//do nothing here
	}

	public function insert(){

		$insertData = $this->escapeData($this->data);

		return $this->db->query("INSERT INTO ".$this->tableName." (".implode(', ', array_keys($insertData)).
								") VALUES (".implode(', ', array_values($insertData)).")");
	}

	private function escapeData($data){

		$escapedData = array();

		foreach ($data as $k => $v){
			$escapedData['`'.$k.'`'] = $this->escape($v);
		}

		return $escapedData;
	}

	private function escape($str){
		if (is_string($str)){
			$str = "'".$this->db->escape_str($str)."'";
		}
		elseif (is_bool($str)){
			$str = ($str === FALSE) ? 0 : 1;
		}
		elseif (is_null($str)){
			$str = 'NULL';
		}

		return $str;
	}

	

}