<?php

class PifReference extends Dao{
	
	protected $primaryKey = 'id';
	protected $tableName = 'pif_ref';

	protected $pifRecord = array(
		'code',
		'val',
		'description'
	);
	
}