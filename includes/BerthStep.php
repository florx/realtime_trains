<?php

class BerthStep extends Dao{
	
	protected $primaryKey = 'id';
	protected $tableName = 'berth_step';

	protected function afterPersist(){
		$mysqli = $this->db->getConn();

		if (!($stmt = $mysqli->prepare("UPDATE rt_services rs
					join activations a on a.train_id = rs.train_id and a.train_uid = rs.train_uid
					join schedules s on s.CIF_train_uid = a.train_uid and s.schedule_start_date <= CURDATE() and s.schedule_end_date >= CURDATE() and s.CIF_stp_indicator = (
						select min(CIF_stp_indicator) from schedules where CIF_train_uid = a.train_uid and schedule_start_date <= CURDATE() and schedule_end_date >= CURDATE() 
					) and rs.schedule_id = s.id
					join berth_step bs on bs.descr = SUBSTRING(a.schedule_wtt_id, 1, 4)
					join smart_locations smla on smla.fromberth = bs.`from` and smla.toberth = bs.`to` and rs.stanox = smla.stanox
					set rs.rt_departure = bs.time,
					rs.rt_departure_id = bs.id
					where bs.id = ? and smla.`event` in ('B', 'D')"))) {
			throw new DaoException ("Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error);
		}

		/* Prepared statement, stage 2: bind and execute */
		if (!$stmt->bind_param("i", $this->id)) {
			throw new DaoException ("Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error);
		}

		if (!$stmt->execute()) {
			throw new DaoException ("Execute failed: (" . $stmt->errno . ") " . $stmt->error);
		}

		if (!($stmt = $mysqli->prepare("UPDATE rt_services rs
					join activations a on a.train_id = rs.train_id and a.train_uid = rs.train_uid
					join schedules s on s.CIF_train_uid = a.train_uid and s.schedule_start_date <= CURDATE() and s.schedule_end_date >= CURDATE() and s.CIF_stp_indicator = (
						select min(CIF_stp_indicator) from schedules where CIF_train_uid = a.train_uid and schedule_start_date <= CURDATE() and schedule_end_date >= CURDATE() 
					) and rs.schedule_id = s.id
					join berth_step bs on bs.descr = SUBSTRING(a.schedule_wtt_id, 1, 4)
					join smart_locations smla on smla.fromberth = bs.`from` and smla.toberth = bs.`to` and rs.stanox = smla.stanox
					set rs.rt_arrival = bs.time,
					rs.rt_arrival_id = bs.id
					where bs.id = ? and smla.`event` in ('A', 'C')"))) {
			throw new DaoException ("Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error);
		}

		/* Prepared statement, stage 2: bind and execute */
		if (!$stmt->bind_param("i", $this->id)) {
			throw new DaoException ("Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error);
		}

		if (!$stmt->execute()) {
			throw new DaoException ("Execute failed: (" . $stmt->errno . ") " . $stmt->error);
		}
	}

}