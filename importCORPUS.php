<?php

require('includes/init.php');

$data = file_get_contents('ref/CORPUSExtract.json');

$json = json_decode($data);

foreach($json->TIPLOCDATA as $oldLocation){

	$location = array_change_key_case((array) $oldLocation, CASE_LOWER);

	$a = new CorpusLocation($location);
	$a->persist();
}