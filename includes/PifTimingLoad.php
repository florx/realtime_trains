<?php

class PifTimingLoad extends Dao{
	
	protected $primaryKey = 'id';
	protected $tableName = 'pif_timing_load';

	protected $pifRecord = array(
		'traction_type',
		'trailing_load',
		'speed',
		'ra_gauge',
		'description',
		'itps_power_type',
		'itps_load',
		'limiting_speed'
	);
	
}
