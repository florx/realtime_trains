<?php

//RENAME this file to config.php, then fill in all of the values below

/* 	Network Rail Stomp Details 

	You'll need an account in 'Active' state from: http://datafeeds.networkrail.co.uk
	See the Wiki for more information: http://nrodwiki.rockshore.net/
*/

define("STOMP_SERVER", "tcp://datafeeds.networkrail.co.uk:61618");
define("STOMP_USER", ""); //usually your email address
define("STOMP_PASS", "");


/*	MySQL Database Details
	
	You'll need to create the schema first using the tables.sql file in the root.
*/

define("DATABASE_SERVER", "localhost");
define("DATABASE_USER", "");
define("DATABASE_PASS", "");
define("DATABASE_DB", ""); //name of the database to connect to