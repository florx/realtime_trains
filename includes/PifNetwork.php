<?php

class PifNetwork extends Dao{
	
	protected $primaryKey = 'id';
	protected $tableName = 'pif_network';

	protected $pifRecord = array(
		'tiploc_origin',
		'tiploc_destination',
		'running_line_code',
		'running_line_desc',
		'start_date',
		'end_date',
		'initial_direction',
		'final_direction',
		'distance',
		'doo_passenger',
		'doo_non_passenger',
		'retb',
		'zone',
		'reversible_line',
		'power_supply_type',
		'ra',
		'max_train_length'
	);
	
}
