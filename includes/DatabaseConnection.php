<?php

class DatabaseConnection{

	private static $_instance = null;
	private $_model = array();
	private $_conn = null;
	
	public static function getInstance(){
		if(is_null(self::$_instance))
			self::$_instance = new self();

		return self::$_instance;
	}

	private function __construct(){
		$this->_connect();
	}

	private function __clone(){}
	private function __wakeup(){}

	private function _connect(){

		$this->_conn = mysqli_connect(DATABASE_SERVER, DATABASE_USER, DATABASE_PASS, DATABASE_DB); 

		if(!$this->_conn){
			throw new DaoException(mysqli_error($this->_conn));
		}
	}

	public function loadModel($tableName){
		$query = $this->query('DESCRIBE ' . $tableName);

		while ($column = $query->fetch_assoc()) {
			$this->model[$tableName][$column['Field']] = (object) $column;
		}

		//print_R($this->model);
	}

	public function getModel($tableName){
		if(!isset($this->model[$tableName]))
			$this->loadModel($tableName);

		return $this->model[$tableName];
	}

	public function getConn(){
		return $this->_conn;
	}

	public function query($sql){

		$query = $this->_conn->query($sql);

		if(!$query)
			throw new DaoException(mysqli_error($this->_conn));

		return $query;
	}

	public function insertID(){
		return $this->_conn->insert_id;
	}

	public function escape_str($str)
	{
		if (is_array($str)){
			foreach ($str as $key => $val){
				$str[$key] = $this->escape_str($val, $like);
			}

			return $str;
		}

		if (function_exists('mysqli_real_escape_string') AND is_object($this->_conn)){
			$str = mysqli_real_escape_string($this->_conn, $str);
		}elseif (function_exists('mysql_escape_string')){
			$str = mysql_escape_string($str);
		}else{
			$str = addslashes($str);
		}

		return $str;
	}
}