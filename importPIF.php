<?php

require('includes/init.php');


$file_handle = fopen('ref/20140116_ReferenceData.pif.txt', "r");
while (!feof($file_handle)) {
	$line = fgets($file_handle);	

	$type = substr($line, 0, 3);
	$line = substr($line, 6);

	

	switch($type){
		case 'REF': 
			$a = new PifReference($line);
			$a->persist();

			break;
		case 'TLD': 
			$a = new PifTimingLoad($line);
			$a->persist();

			break;
		case 'LOC': 
			$a = new PifLocation($line);
			$a->persist();

			break;
		case 'PLT': 
			$a = new PifPlatform($line);
			$a->persist();

			break;
		case 'NWK': 
			$a = new PifNetwork($line);
			$a->persist();

			break;
		case 'TLK': 
			$a = new PifTiming($line);
			$a->persist();

			break;
		default:
			echo 'Skipping ' . $type . "\n";
			break;
	}
	
}
fclose($file_handle);
