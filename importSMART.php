<?php

require('includes/init.php');

$data = file_get_contents('ref/SMARTExtract.json');

$json = json_decode($data);

foreach($json->BERTHDATA as $oldLocation){

	$location = array_change_key_case((array) $oldLocation, CASE_LOWER);

	$a = new SmartLocation($location);
	$a->persist();
}